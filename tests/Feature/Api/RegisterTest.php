<?php

namespace Tests\Feature\Api;

use App\Models\Entity;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test registration required fields
     * @group register
     * @test
     */
    public function register_required_fields()
    {
        $this->postJson(route('api.register'))
            ->assertJsonStructure(['message' , 'errors' => ['name','email' , 'password' , 'type' , 'document']])
            ->assertStatus(422);
    }

    /**
     * Test document is unique
     * @group register
     * @test
     */
    public function document_and_email_is_unique()
    {
        $user = factory(User::class)->create();

        $entity = factory(Entity::class)->create([
            'user_id' => $user->id
        ]);

        $payload = [
            'name' => 'Teste',
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'type' => 'PF',
            'document' => $entity->document,
        ];

        $this->postJson(route('api.register') , $payload)
            ->assertStatus(422)
            ->assertJsonStructure(['message' , 'errors' => ['document' , 'email']]);

        $this->assertDatabaseMissing('users' , [
            'name' => $payload['name'],
            'email' => $payload['email']
        ]);
    }


    /**
     * Test registration successful
     * @group register
     * @test
     */
    public function registration_successful()
    {
        $user = factory(User::class)->make();

        $entity = factory(Entity::class)->make();

        $payload = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'type' => $entity->type,
            'document' => $entity->document,
        ];

        $this->postJson(route('api.register') , $payload)
            ->assertJsonStructure(['access_token' , 'token_type' , 'expires_in']);

        $this->assertDatabaseHas('users' , $user->only('name' , 'email'));
        $this->assertDatabaseHas('entities' , $entity->only('document'));
        $this->assertDatabaseHas('wallets' , [
            'available_balance' => 0
        ]);
    }
}
