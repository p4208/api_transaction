<?php

namespace Tests\Feature\Api;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

use function PHPUnit\Framework\assertJson;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test required fields
     * @group auth
     * @test
     */
    public function authentication_required_fields()
    {
        $this->postJson(route('api.login'))->assertJsonStructure([
            'message' , 'errors' => ['email' , 'password']
        ]);
    }

    /**
     * Test password invalid
     * @group auth
     * @test
     * @return void
     */
    public function password_invalid()
    {
        $user = factory(User::class)->create();

        $this->postJson(route('api.login') , [
            'email' => $user->email,
            'password' => '987654321'
        ])
        ->assertJson([
            'message' => 'Invalid email or password',
        ])
        ->assertStatus(401);
    }

    /**
     * Test login successful
     * @group auth
     * @test
     * @return void
     */
    public function login_successful()
    {
        $user = factory(User::class)->create();

        $this->postJson(route('api.login') , [
            'email' => $user->email,
            'password' => '12345678'
        ])
        ->assertJsonStructure(['access_token' , 'token_type' , 'expires_in']);
    }

    /**
     * Test logout successful
     * @group auth
     * @test
     * @return void
     */
    public function logout_successful()
    {
        $user = factory(User::class)->create();

        $token = JWTAuth::fromUser($user);

        $this->withHeader('Authorization', "Bearer {$token}")
            ->postJson(route('api.logout'))
            ->assertJson(['message' => 'successfully logged out']);
    }
}
