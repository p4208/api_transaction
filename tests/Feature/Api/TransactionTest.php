<?php

namespace Tests\Feature\Api;

use App\Events\TransactionNotification;
use App\Models\Entity;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class TransactionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test registration required fields
     * @group transaction
     * @test
     */
    public function transaction_required_fields()
    {
        $user = factory(User::class)->create();

        $token = JWTAuth::fromUser($user);

        $this->withHeader('Authorization', "Bearer {$token}")
            ->postJson(route('api.transactions.store'))
            ->assertJsonStructure(['message' , 'errors' => ['payee' , 'value']])
            ->assertStatus(422);
    }

    /**
     * Test transaction between users
     * @group transaction
     * @test
     */
    public function transaction_between_users()
    {
        Event::fake();

        $payer = factory(User::class)->create();
        $payee = factory(User::class)->create();

        factory(Entity::class)->create(['user_id' => $payer->id , 'type' => 'PF']);
        $wallet_payer = factory(Wallet::class)->create(['entity_id' => $payer->entity->id , 'available_balance' => 10000]);

        factory(Entity::class)->create(['user_id' => $payee->id , 'type' => 'PF']);
        $wallet_payee = factory(Wallet::class)->create(['entity_id' => $payee->entity->id , 'available_balance' => 1000]);

        $token = JWTAuth::fromUser($payer);

        $value = (5000 / 100);

        $this->withHeader('Authorization', "Bearer {$token}")
            ->postJson(route('api.transactions.store') , [
                'payee' => $payee->id,
                'value' => $value
            ])->assertJson([
                'message' => 'Successful transaction'
            ])->assertSuccessful();

        $this->assertDatabaseHas('transactions' , [
            'payer_wallet_id' => $wallet_payer->id,
            'payee_wallet_id' => $wallet_payee->id,
            'value' => ($value * 100)
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payer->id,
            'available_balance' => $wallet_payer->available_balance - ($value * 100)
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payee->id,
            'available_balance' => $wallet_payee->available_balance + ($value * 100)
        ]);

        Event::assertDispatched(TransactionNotification::class, function ($event) use($wallet_payer){
            return $event->transaction->payer_wallet_id === $wallet_payer->id;
        });
    }

    /**
     * Test transaction between user and storekeeper
     * @group transaction
     * @test
     */
    public function transaction_between_user_and_company()
    {
        Event::fake();

        $payer = factory(User::class)->create();
        $payee = factory(User::class)->create();

        factory(Entity::class)->create(['user_id' => $payer->id , 'type' => 'PF']);
        $wallet_payer = factory(Wallet::class)->create(['entity_id' => $payer->entity->id , 'available_balance' => 10000]);

        factory(Entity::class)->create(['user_id' => $payee->id , 'type' => 'PJ']);
        $wallet_payee = factory(Wallet::class)->create(['entity_id' => $payee->entity->id , 'available_balance' => 1000]);

        $token = JWTAuth::fromUser($payer);

        $value = (5000 / 100);

        $this->withHeader('Authorization', "Bearer {$token}")
            ->postJson(route('api.transactions.store') , [
                'payee' => $payee->id,
                'value' => $value
            ])->assertJson([
                'message' => 'Successful transaction'
            ])->assertSuccessful();

        $this->assertDatabaseHas('transactions' , [
            'payer_wallet_id' => $wallet_payer->id,
            'payee_wallet_id' => $wallet_payee->id,
            'value' => ($value * 100)
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payer->id,
            'available_balance' => $wallet_payer->available_balance - ($value * 100)
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payee->id,
            'available_balance' => $wallet_payee->available_balance + ($value * 100)
        ]);

        Event::assertDispatched(TransactionNotification::class, function ($event) use($wallet_payer){
            return $event->transaction->payer_wallet_id === $wallet_payer->id;
        });

    }

    /**
     * Test transaction between company and user
     * @group transaction
     * @test
     */
    public function transaction_between_company_and_user()
    {
        $payer = factory(User::class)->create();
        $payee = factory(User::class)->create();

        factory(Entity::class)->create(['user_id' => $payer->id , 'type' => 'PJ']);
        $wallet_payer = factory(Wallet::class)->create(['entity_id' => $payer->entity->id , 'available_balance' => 10000]);

        factory(Entity::class)->create(['user_id' => $payee->id , 'type' => 'PF']);
        $wallet_payee = factory(Wallet::class)->create(['entity_id' => $payee->entity->id , 'available_balance' => 1000]);

        $token = JWTAuth::fromUser($payer);

        $value = (5000 / 100);

        $this->withHeader('Authorization', "Bearer {$token}")
            ->postJson(route('api.transactions.store') , [
                'payee' => $payee->id,
                'value' => $value
            ])->assertJson([
                'message' => 'Permission denied'
            ])->assertStatus(403);

        $this->assertDatabaseMissing('transactions' , [
            'payer_wallet_id' => $wallet_payer->id,
            'payee_wallet_id' => $wallet_payee->id,
            'value' => ($value * 100)
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payer->id,
            'available_balance' => $wallet_payer->available_balance
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payee->id,
            'available_balance' => $wallet_payee->available_balance
        ]);
    }

    /**
     * Test transaction between same users
     * @group transaction
     * @test
     */
    public function transaction_between_same_users()
    {
        $payer = factory(User::class)->create();

        factory(Entity::class)->create(['user_id' => $payer->id , 'type' => 'PF']);
        $wallet_payer = factory(Wallet::class)->create(['entity_id' => $payer->entity->id , 'available_balance' => 10000]);

        $token = JWTAuth::fromUser($payer);

        $value = (5000 / 100);

        $this->withHeader('Authorization', "Bearer {$token}")
            ->postJson(route('api.transactions.store') , [
                'payee' => $payer->id,
                'value' => $value
            ])->assertJson([
                'message' => 'Ops! This is not authorized'
            ])->assertStatus(403);

        $this->assertDatabaseMissing('transactions' , [
            'payer_wallet_id' => $wallet_payer->id,
            'payee_wallet_id' => $wallet_payer->id,
            'value' => ($value * 100)
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payer->id,
            'available_balance' => $wallet_payer->available_balance
        ]);
    }

    /**
     * Test transaction when balance available is insufficient
     * @group transaction
     * @test
     */
    public function transaction_when_balance_available_is_insufficient()
    {
        $payer = factory(User::class)->create();
        $payee = factory(User::class)->create();

        factory(Entity::class)->create(['user_id' => $payer->id , 'type' => 'PF']);
        $wallet_payer = factory(Wallet::class)->create(['entity_id' => $payer->entity->id , 'available_balance' => 3000]);

        factory(Entity::class)->create(['user_id' => $payee->id , 'type' => 'PF']);
        $wallet_payee = factory(Wallet::class)->create(['entity_id' => $payee->entity->id , 'available_balance' => 1000]);

        $token = JWTAuth::fromUser($payer);

        $value = (5000 / 100);

        $this->withHeader('Authorization', "Bearer {$token}")
            ->postJson(route('api.transactions.store') , [
                'payee' => $payee->id,
                'value' => $value
            ])->assertJson([
                'message' => 'Your available balance is insufficient'
            ])->assertStatus(403);

        $this->assertDatabaseMissing('transactions' , [
            'payee_wallet_id' => $wallet_payer->id,
            'value' => ($value * 100)
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payer->id,
            'available_balance' => $wallet_payer->available_balance
        ]);

        $this->assertDatabaseHas('wallets' , [
            'id' => $wallet_payee->id,
            'available_balance' => $wallet_payee->available_balance
        ]);
    }
}
