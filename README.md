# API de transações

Exemplo de API Rest com Laravel, com o objetivo de criar um sistema de transações entre 2 entidades

### Objetivo do projeto

Temos 2 tipos de usuários, os comuns e lojistas, ambos têm carteira com dinheiro e realizam transferências entre eles.

Requisitos:

- Para ambos tipos de usuário, precisamos do Nome Completo, CPF, e-mail e Senha. CPF/CNPJ e e-mails devem ser únicos no sistema. Sendo assim, seu sistema deve permitir apenas um cadastro com o mesmo CPF ou endereço de e-mail.

- Usuários podem enviar dinheiro (efetuar transferência) para lojistas e entre usuários.

- Lojistas só recebem transferências, não enviam dinheiro para ninguém.

- Antes de finalizar a transferência, deve-se consultar um serviço autorizador externo

- A operação de transferência deve ser uma transação (ou seja, revertida em qualquer caso de inconsistência) e o dinheiro deve voltar para a carteira do usuário que envia.

- No recebimento de pagamento, o usuário ou lojista precisa receber notificação (envio de email, sms) enviada por um serviço de terceiro e eventualmente este serviço pode estar indisponível/instável.


#### Containers

| Container             | Descrição                | Porta |
|-----------------------|----------------------|------|
| **apitransaction-nginx** | `nginx:alpine` | 8000 |
| **apitransaction-db** | `mysql:5.7` | - |
| **apitransaction-phpmyadmin** | `phpmyadmin:latest` | 8081 |

## Instalação

#### Configurando o ambiente local usando Docke:

```bash
# Faça a cópia do repositório
$ git clone https://github.com/Devitibeck/api_transaction.git && cd api_transaction

# Copie o arquivo .env.example pra .env
$ cp .env.example .env
o .env.example já está configurado com as variáveis do docker

# Construa os containers da aplicação
$ docker-compose build app

# Inicie a aplicação em background
$ docker-compose up -d

# Instale as dependências utilizando o composer
$ docker-compose exec app composer install

# Gere a chave
$ docker-compose exec app php artisan key:generate

# Gere a chave do JWT
$ docker-compose exec app php artisan jwt:secret

# Rode as migrações
$ docker-compose exec app php artisan migrate

# Acessar a aplicação
Acesse a aplicação na URL http://localhost:8000

# Caso queira rodar os testes
$ docker-compose exec app php artisan test
```
## Endpoints

````POST api/register````

Para se cadastrar no sistema:

````
{
    'name': 'User example',
    'email': 'userexample@gmail.com',
    'type' : 'PF',
    'document' : '416.431.650-03',
    'password': '12345678',
    'password_confirmation' : '12345678'
}
````

Exemplo de resposta:

````
{
    'access_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N...',
    'token_type': 'bearer',
    'expires_in': 3600
}
````

````POST api/login````

Para se logar no sistema:

````
{
    'email': 'userexample@gmail.com',
    'password': '12345678',
}
````

Exemplo de resposta:

````
{
    'access_token': "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N...",
    'token_type': "bearer",
    'expires_in': 3600
}
````

````POST api/logout````

Para se deslogar do sistema:


Exemplo de resposta:

````
{
    'message': 'successfully logged out'
}
````


````GET api/wallet````

Para visualizar todas as informações da carteira:

Exemplo de resposta:

````
{
    'id": 1,
    'entity_id': 1,
    'available_balance': 5000,
    'deleted_at': null,
    'created_at': '2021-05-18T18:58:03.000000Z',
    'updated_at': '2021-05-18T19:08:09.000000Z'
}
````

````GET api/wallet/history````

Para visualizar os históricos de transações

Exemplo de resposta:

````
{
    'id": 1,
    'entity_id': 1,
    'available_balance': 5000,
    'deleted_at': null,
    'created_at': '2021-05-18T18:58:03.000000Z',
    'updated_at': '2021-05-18T19:08:09.000000Z'
    'received': [
        {
            'id': 4,
            'payer_wallet_id': 2,
            'payee_wallet_id': 1,
            'value': 5000,
            'deleted_at': null,
            'created_at': '2021-05-18T19:08:09.000000Z',
            'updated_at': '2021-05-18T19:08:09.000000Z'
        }
    ],
    'sent': [
        {
            'id': 1,
            'payer_wallet_id': 1,
            'payee_wallet_id': 2,
            'value': 10,
            'deleted_at': null,
            'created_at': '2021-05-18T19:04:04.000000Z',
            'updated_at': '2021-05-18T19:04:04.000000Z'
        }
    ]
}
````

````POST api/transactions````

Para realizar uma nova transação:

````
{
    'payee': 1,
    'value': 50.0
}
````

Exemplo de resposta:

````
{
    'message': 'Successful transaction'
}
````
