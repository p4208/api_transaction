<?php

namespace App\Services;

use App\Repositories\Wallet\WalletRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class WalletService
{
    /**
     * Returns the authenticated user's wallet, showing all their information
     * @return json
     */
    public function retrieve() : Model
    {
        return auth()->user()->entity->wallet;
    }

    /**
     * Returns the transaction history of the authenticated user's wallet
     * @return json
     */
    public function history() : Model
    {
        return $this->retrieve()->load(['received', 'sent']);
    }
}
