<?php

namespace App\Services;

use App\Events\TransactionNotification;
use App\Exceptions\InsufficientBalanceException;
use App\Exceptions\TransactionNotAllowedException;
use App\Repositories\Transaction\TransactionRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Wallet\WalletRepositoryInterface;
use App\Utils\Guzzle;
use Exception;
use Illuminate\Support\Facades\DB;

class TransactionService
{
    private $transactionRepository;
    private $walletRepository;
    private $userRepository;
    private $guzzle;

    public function __construct(
        TransactionRepositoryInterface $transactionRepository ,
        WalletRepositoryInterface $walletRepository,
        UserRepositoryInterface $userRepository,
        Guzzle $guzzle
    )
    {
        $this->transactionRepository = $transactionRepository;
        $this->walletRepository = $walletRepository;
        $this->userRepository = $userRepository;
        $this->guzzle = $guzzle;
    }

    /**
     * Create a new transaction between two entities
     */
    public function create(int $payee_id , float $value)
    {
        DB::beginTransaction();

        try{
            $payer = auth()->user();
            $payer_wallet = $payer->entity->wallet;

            $payee = $this->userRepository->find($payee_id);

            $payee_wallet = $payee->entity->wallet;

            throw_if($payer->entity->type != 'PF' , new TransactionNotAllowedException('Permission denied' , 403));

            throw_if($payer->id == $payee->id , new TransactionNotAllowedException('Ops! This is not authorized' , 403));

            throw_if($payer_wallet->available_balance < ($value * 100) , new InsufficientBalanceException('Your available balance is insufficient' , 403));

            throw_if(!$this->guzzle->validateTransaction() , new TransactionNotAllowedException('Could not validate the transaction' , 403));

            $transaction = $this->transactionRepository->create([
                'payer_wallet_id' => $payer_wallet->id,
                'payee_wallet_id' => $payee_wallet->id,
                'value' => ($value * 100)
            ]);

            $this->walletRepository->updateBalance($transaction);

            event(new TransactionNotification($transaction));

            DB::commit();
            return $transaction;
        }catch(TransactionNotAllowedException $e){
            DB::rollback();
            throw new TransactionNotAllowedException($e->getMessage() , $e->getCode());
        }catch(InsufficientBalanceException $e){
            DB::rollback();
            throw new InsufficientBalanceException($e->getMessage() , $e->getCode());
        }catch(Exception $e){
            DB::rollback();
            throw new Exception($e->getMessage() , $e->getCode());
        }
    }
}
