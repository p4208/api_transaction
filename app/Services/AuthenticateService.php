<?php

namespace App\Services;

class AuthenticateService
{
    /**
     * authentic the user
     * @return json
     */
    public function login(array $credentials , $guard = null)
    {
        if (!$token = auth($guard ?: config('auth.defaults.guard'))->attempt($credentials)) {
            return response()->error('Invalid email or password', 401);
        }

        return response()->token($token);
    }
}
