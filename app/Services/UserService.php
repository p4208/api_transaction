<?php

namespace App\Services;

use App\Repositories\Entity\EntityRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Wallet\WalletRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class UserService
{
    private $userRepository;
    private $entityRepository;
    private $walletRepository;

    public function __construct(
        UserRepositoryInterface $userRepository ,
        EntityRepositoryInterface $entityRepository,
        WalletRepositoryInterface $walletRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->entityRepository = $entityRepository;
        $this->walletRepository = $walletRepository;
    }

    /**
     * retrieve the authenticated user
     * @return User
     */
    public function retrieve()
    {
        return $this->userRepository->retrieve();
    }

    /**
     * Create a new user
     * @return User
     */
    public function create(
        string $name,
        string $email,
        string $password,
        string $type,
        string $document
    )
    {
        DB::beginTransaction();
        try{
            $user = $this->userRepository->create([
                'name' => $name,
                'email' => $email,
                'password' => bcrypt($password)
            ]);

            $entity = $this->entityRepository->create([
                'user_id' => $user->id,
                'type' => $type,
                'document' => $document
            ]);

            $wallet = $this->walletRepository->create([
                'entity_id' => $entity->id
            ]);

            DB::commit();
            return $user;
        }catch(Exception $e){
            DB::rollback();
            throw new Exception($e->getMessage() , $e->getCode());
        }
    }
}
