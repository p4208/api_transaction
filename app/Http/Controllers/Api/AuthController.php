<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Services\AuthenticateService;
use App\Services\UserService;
use Exception;

class AuthController extends Controller
{
    private $authenticateService;
    private $userService;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(AuthenticateService $authenticateService , UserService $userService)
    {
        $this->authenticateService = $authenticateService;
        $this->userService = $userService;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try{
            return $this->authenticateService->login($credentials);
        }catch(Exception $e){
            return response()->internalError($e);
        }
    }

    public function logout()
    {
        auth()->logout();
        return response()->success([
            'message' => 'successfully logged out'
        ]);
    }

    public function me()
    {
        return $this->userService->retrieve();
    }
}
