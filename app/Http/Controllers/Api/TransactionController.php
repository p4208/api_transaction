<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InsufficientBalanceException;
use App\Exceptions\TransactionNotAllowedException;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTransactionRequest;
use App\Services\TransactionService;
use Exception;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTransactionRequest $request)
    {
        try{
            $this->transactionService->create($request->payee , $request->value);

            return response()->success([
                'message' => 'Successful transaction'
            ]);
        }catch(InsufficientBalanceException $e){
            return response()->error($e->getMessage() , $e->getCode());
        }catch(TransactionNotAllowedException $e){
            return response()->error($e->getMessage() , $e->getCode());
        }catch(Exception $e){
            return response()->internalError($e);
        }
    }
}
