<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Services\AuthenticateService;
use App\Services\UserService;
use Throwable;

class RegisterController extends Controller
{
    private $userService;
    private $authenticateService;

    public function __construct(UserService $userService , AuthenticateService $authenticateService)
    {
        $this->userService = $userService;
        $this->authenticateService = $authenticateService;
    }

    public function register(RegisterRequest $request)
    {
        try{
            $user = $this->userService->create(
                $request->name ,
                $request->email ,
                (string) $request->password ,
                $request->type ,
                $request->document
            );
            return $this->authenticateService->login([
                'email' => $user->email,
                'password' => $request->password
            ]);
        }catch(Throwable $t){
            return response()->internalError($t);
        }
    }
}
