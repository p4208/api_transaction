<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class ApiProtectedRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            JWTAuth::parseToken()->authenticate();
        }catch(TokenInvalidException $e){
            return response()->error('Invalid token' , 401);
        }catch(TokenExpiredException $e){
            return response()->error('Expired token' , 401);
        }catch(Exception $e){
            return response()->error('Token not found' , 401);
        }

        return $next($request);
    }
}
