<?php

namespace App\Utils;

use Exception;
use GuzzleHttp\Client;
use InvalidArgumentException;

class Guzzle
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function sendNotification(): bool
    {
        $endpoint = env('API_TRANSFER_NOTIFICATION');

        throw_if(!$endpoint , new InvalidArgumentException('Invalid notification URL'));

        $result = $this->endpoint($endpoint);

        return strtolower($result->message) == 'enviado';
    }

    public function validateTransaction(): bool
    {
        $endpoint = env('API_TRANSFER_REQUEST');

        throw_if(!$endpoint , new InvalidArgumentException('Invalid validation URL'));

        $result = $this->endpoint($endpoint);

        return strtolower($result->message) == 'autorizado';
    }

    public function endpoint($url)
    {
        try {
            $response = $this->client->request('GET', $url);
        } catch (Exception $e) {
            throw new Exception($e->getMessage() , $e->getCode());
        }

        return $this->responseHandler($response->getBody()->getContents());
    }

    public function responseHandler($response)
    {
        if ($response) {
            return json_decode($response);
        }

        return null;
    }
}
