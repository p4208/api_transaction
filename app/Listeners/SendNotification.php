<?php

namespace App\Listeners;

use App\Events\TransactionNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Utils\Guzzle;
use Exception;

class SendNotification implements ShouldQueue
{
    use InteractsWithQueue;

    private $guzzle;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Guzzle $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * Handle the event.
     *
     * @param  TransactionNotification  $event
     * @return void
     */
    public function handle(TransactionNotification $event)
    {
        if (!$this->guzzle->sendNotification()) {
            throw new Exception('Could not finish the transaction');
        }
    }
}
