<?php
namespace App\Repositories\Wallet;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Model;

interface WalletRepositoryInterface
{
    public function create(array $attributes) : Model;

    public function updateBalance(Transaction $transaction) : void;
}
