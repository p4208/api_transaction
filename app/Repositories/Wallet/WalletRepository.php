<?php

namespace App\Repositories\Wallet;

use App\Models\Transaction;
use App\Models\Wallet;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class WalletRepository extends BaseRepository implements WalletRepositoryInterface
{
    protected $model = Wallet::class;

    public function updateBalance(Transaction $transaction): void
    {
        $transaction->payer()->decrement('available_balance' , $transaction->value);
        $transaction->payee()->increment('available_balance' , $transaction->value);
    }
}
