<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = $this->resolveModel();
    }

    public function resolveModel(){
        return app($this->model);
    }

    public function all($columns = ['*']) : Collection
    {
        return $this->model->get($columns);
    }

    public function findWhere(array $where, $columns = ['*']) : Collection
    {
        return $this->model->where($where)->get($columns);
    }

    public function findWhereIn($field, array $values, $columns = ['*']) : Collection
    {
        return $this->model->whereIn($field , $values)->get($columns);
    }

    public function find($id, $columns = ['*']) : ?Model
    {
        return $this->model->findOrFail($id , $columns);
    }


    public function create(array $attributes) : Model
    {
        return $this->model->create($attributes);
    }

    public function update(array $attributes, $id) : bool
    {
        return $this->find($id)->update($attributes);

    }

    public function delete($id) : bool
    {
        return $this->find($id)->delete();
    }

    public function orderBy($column, $direction = 'asc')
    {
        $this->model = $this->model->orderBy($column, $direction);

        return $this;
    }
}
