<?php

namespace App\Repositories\User;

use Illuminate\Database\Eloquent\Model;

interface UserRepositoryInterface
{
    public function retrieve() : Model;

    public function create(array $attributes) : Model;

    public function find($id, $columns = ['*']) : ?Model;
}
