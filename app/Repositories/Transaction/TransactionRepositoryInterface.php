<?php
namespace App\Repositories\Transaction;

use Illuminate\Database\Eloquent\Model;

interface TransactionRepositoryInterface
{
    public function create(array $attributes) : Model;
}
