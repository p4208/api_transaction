<?php

namespace App\Repositories\Entity;

use App\Models\Entity;
use App\Repositories\BaseRepository;

class EntityRepository extends BaseRepository implements EntityRepositoryInterface
{
    protected $model = Entity::class;
}
