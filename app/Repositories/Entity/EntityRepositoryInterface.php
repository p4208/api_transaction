<?php
namespace App\Repositories\Entity;

use Illuminate\Database\Eloquent\Model;

interface EntityRepositoryInterface
{
    public function create(array $attributes) : Model;
}
