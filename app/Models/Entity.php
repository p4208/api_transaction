<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Entity
 * @property integer $user_id
 * @property string $type
 * @property string $document
 */
class Entity extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id' , 'type' ,'document'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }
}
