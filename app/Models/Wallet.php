<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Wallet
 * @property integer $entity_id
 * @property integer $available_balance
 */
class Wallet extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'entity_id' , 'available_balance'
    ];

    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    public function received()
    {
        return $this->hasMany(Transaction::class, 'payee_wallet_id');
    }

    public function sent()
    {
        return $this->hasMany(Transaction::class, 'payer_wallet_id');
    }
}
