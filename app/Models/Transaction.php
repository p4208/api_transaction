<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @property integer $payer_wallet_id
 * @property integer $payee_wallet_id
 * @property integer $value
 */
class Transaction extends Model
{
    protected $fillable  = [
        'payer_wallet_id' , 'payee_wallet_id' , 'value'
    ];

    public function payer()
    {
        return $this->belongsTo(Wallet::class, 'payer_wallet_id');
    }

    public function payee()
    {
        return $this->belongsTo(Wallet::class, 'payee_wallet_id');
    }
}
