<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Throwable;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('token' , function($token){
            return Response::json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ]);
        });

        Response::macro('internalError' , function(Throwable $e , int $status_code = 500){
            Log::error("Invalid settings" , [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);

            return Response::json([
                'message' => 'An internal error occurred'
            ], $status_code);
        });

        Response::macro('success' , function($data , int $status_code = 200){
            return Response::json($data, $status_code)->original;
        });

        Response::macro('error' , function(string $message , int $status_code){
            return Response::json([
                'message' => $message
            ], $status_code);
        });

        Response::macro('errorValidation' , function($errors){
            return Response::json([
                'message' => 'The given data was invalid.',
                'errors' => $errors
            ], 422);
        });
    }
}
