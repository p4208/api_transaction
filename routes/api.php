<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->namespace('Api')->group(function(){
    Route::group(['middleware' => 'api'], function ($router) {
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('register', 'RegisterController@register')->name('register');
    });

    Route::middleware('apiJwt')->group(function () {
        Route::post('logout', 'AuthController@logout')->name('logout');
        Route::post('transactions', 'TransactionController@store')->name('transactions.store');

        Route::group(['prefix' => 'wallet'], function() {
            Route::get('/' , 'WalletController@retrieve')->name('wallet.retrieve');
            Route::get('/history' , 'WalletController@history')->name('wallet.history');
        });
    });
});

