<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Entity;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Entity::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
    $faker->addProvider(new \Faker\Provider\pt_BR\Company($faker));
    return [
        'user_id' =>  factory(User::class)->create()->id,
        'type' => $faker->randomElement(['PJ', 'PF']),
        'document' =>  $faker->cnpj(),
    ];
});

$factory->state(Entity::class, 'PF', function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
    $faker->addProvider(new \Faker\Provider\pt_BR\Company($faker));
    return [
        'type' => 'PF',
        'document' =>  $faker->cpf(),
    ];
});

$factory->state(Entity::class, 'PJ', function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
    $faker->addProvider(new \Faker\Provider\pt_BR\Company($faker));
    return [
        'type' => 'PJ',
        'document' => $faker->cnpj(),
    ];
});

