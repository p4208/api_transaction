<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Entity;
use App\Models\Wallet;
use Faker\Generator as Faker;

$factory->define(Wallet::class, function (Faker $faker) {
    return [
        'entity_id' => factory(Entity::class)->create()->id,
        'available_balance' => $faker->numberBetween(11111,99999)
    ];
});
